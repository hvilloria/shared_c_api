# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170530150737) do

  create_table "academic_days", force: :cascade do |t|
    t.date     "schedule",         null: false
    t.integer  "status"
    t.integer  "position"
    t.integer  "academic_week_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "day_id"
    t.index ["academic_week_id"], name: "index_academic_days_on_academic_week_id"
    t.index ["day_id"], name: "index_academic_days_on_day_id"
  end

  create_table "academic_notes", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "author"
    t.date     "post"
    t.integer  "academic_day_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["academic_day_id"], name: "index_academic_notes_on_academic_day_id"
  end

  create_table "academic_weeks", force: :cascade do |t|
    t.integer  "position",     null: false
    t.integer  "promotion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "week_id"
    t.index ["promotion_id"], name: "index_academic_weeks_on_promotion_id"
    t.index ["week_id"], name: "index_academic_weeks_on_week_id"
  end

  create_table "blackdays", force: :cascade do |t|
    t.string   "name",         null: false
    t.date     "day",          null: false
    t.integer  "promotion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["promotion_id"], name: "index_blackdays_on_promotion_id"
  end

  create_table "days", force: :cascade do |t|
    t.string   "content",    null: false
    t.integer  "position",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "week_id"
    t.index ["week_id"], name: "index_days_on_week_id"
  end

  create_table "notes", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "author"
    t.date     "post_date"
    t.integer  "academic_note_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "day_id"
    t.index ["academic_note_id"], name: "index_notes_on_academic_note_id"
    t.index ["day_id"], name: "index_notes_on_day_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "username",   null: false
    t.string   "password",   null: false
    t.string   "email",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.string   "name",       null: false
    t.date     "start",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "type_of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weeks", force: :cascade do |t|
    t.string   "content",    null: false
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
